#!/bin/sh
set -e

echo "updating Devuan mirror list"
utils/get_devuan_mirrors.pl > data/templates/Devuan.mirrors
